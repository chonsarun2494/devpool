package gorm

import (
	"fmt"
	"log"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

type DB struct {
	db *gorm.DB
}

// สร้าง table Book
type Book struct {
	ID          int     `db:"id" gorm:"primaryKey;autoIncrement"`
	Title       string  `db:"title"`
	Author      string  `db:"author"`
	Description string  `db:"description"`
	Price       float64 `db:"price"`
}
type Users struct {
	ID       uint   `db:"id" gorm:"primaryKey;autoIncrement"`
	Username string `db:"username" gorm:"uniqueIndex;not null"`
	Password string `db:"password"`
	Role     string `db:"role"`
}

func NewDB() (*DB, error) {
	url := "host=postgresql user=peagolang password=supersecret dbname=peagolang port=5432 sslmode=disable"
	//url := os.Getenv("DATABASE_URL")
	db, err := gorm.Open(postgres.Open(url), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})
	if err != nil {
		return nil, err
	}
	return &DB{db: db}, nil
}
func (db *DB) Reset() error {
	err := db.db.Migrator().DropTable(
		&Book{},
		&Users{},
	)
	if err != nil {
		log.Fatal(err)
	}
	return err
}

func (db *DB) AutoMigrate() error {
	err := db.db.Migrator().AutoMigrate(
		&Book{}, &Users{},
	)
	if err != nil {
		log.Fatal(err)
	}

	var books = []Book{
		{Title: "Book Title 1", Author: "Author A", Description: "Description for Book 1", Price: 10.5},
		{Title: "Book Title 2", Author: "Author B", Description: "Description for Book 2", Price: 12.5},
		{Title: "Book Title 3", Author: "Author C", Description: "Description for Book 3", Price: 15.0},
		{Title: "Book Title 4", Author: "Author D", Description: "Description for Book 4", Price: 20.0},
		{Title: "Book Title 5", Author: "Author E", Description: "Description for Book 5", Price: 18.5},
		{Title: "Book Title 6", Author: "Author F", Description: "Description for Book 6", Price: 22.5},
		{Title: "Book Title 7", Author: "Author G", Description: "Description for Book 7", Price: 24.0},
		{Title: "Book Title 8", Author: "Author H", Description: "Description for Book 8", Price: 30.0},
		{Title: "Book Title 9", Author: "Author I", Description: "Description for Book 9", Price: 28.5},
		{Title: "Book Title 10", Author: "Author J", Description: "Description for Book 10", Price: 32.5},
	}
	result := db.Create(&books)
	if result.Error != nil {
		fmt.Println("Error inserting records:", result.Error)
	} else {
		fmt.Println("Records inserted successfully")
	}
	return err
}

// wrapper function Create
func (d *DB) Create(value interface{}) *gorm.DB {
	return d.db.Create(value)
}

// wrapper function Find
func (d *DB) Find(out interface{}, where ...interface{}) *gorm.DB {
	return d.db.Find(out, where...)
}

// wrapper function Update
func (d *DB) Update(value interface{}) *gorm.DB {
	return d.db.Save(value)
}

// wrapper function delete
func (d *DB) Delete(value interface{}, where ...interface{}) *gorm.DB {
	return d.db.Delete(value, where...)
}

// wrapper function GetByID
func (d *DB) First(out interface{}, where ...interface{}) *gorm.DB {
	return d.db.First(out, where...)
}

package main

import (
	"fmt"
	"log"

	"github.com/gin-gonic/gin"
	"gitlab.com/chonsarun2494/bookstore/middleware"
	"gitlab.com/chonsarun2494/bookstore/modules/book/handler"
	"gitlab.com/chonsarun2494/bookstore/modules/book/repository"
	"gitlab.com/chonsarun2494/bookstore/modules/book/services"
	"gitlab.com/chonsarun2494/bookstore/modules/users/user_handler"
	"gitlab.com/chonsarun2494/bookstore/modules/users/users_repository"
	"gitlab.com/chonsarun2494/bookstore/modules/users/users_service"
	"gitlab.com/chonsarun2494/bookstore/pkg/gorm"
	"gitlab.com/chonsarun2494/bookstore/pkg/jwt"
)

func main() {
	// 1. Create a database instance
	db, err := gorm.NewDB()
	if err != nil {
		fmt.Println("Error connecting to the database:", err)
		return
	}
	//Reset Db
	if err := db.Reset(); err != nil {
		log.Fatal(err)
	}
	//Migration
	if err := db.AutoMigrate(); err != nil {
		log.Fatal(err)
	}

	// book
	bookRepo := repository.NewBookRepository(db)
	bookservice := services.NewBookService(bookRepo)
	bookhandler := handler.NewBookHandler(bookservice)
	//users
	userRepo := users_repository.NewUserRepository(db)
	users_service := users_service.NewUsersService(userRepo)
	userhandler := user_handler.NewUsersHandler(users_service)

	r := gin.Default()

	//cors setting
	r.Use(middleware.CORSMiddleware())

	r.GET("/book", jwt.CheckAuthen(), bookhandler.GetAllBookHandler())
	r.GET("/book/:id", jwt.CheckAuthen(), bookhandler.GetBookByIdHandler())
	r.DELETE("/book/:id", jwt.CheckAuthen(), bookhandler.DeleteBookHandler())
	r.POST("/book", jwt.CheckAuthen(), bookhandler.NewBookHandler())
	r.PUT("/book/:id", jwt.CheckAuthen(), bookhandler.UpdateBookHandler())

	// authen route
	r.POST("/register", userhandler.RegisterUser())
	r.POST("/login", userhandler.CheckLoginHandler())

	r.Run(":8328")

}

package services

import (
	"log"

	"gitlab.com/chonsarun2494/bookstore/modules/book"
)

type bookService struct {
	bookRepository book.BookRepository
}

func NewBookService(bookRepository book.BookRepository) bookService {

	return bookService{bookRepository: bookRepository}

} // สร้าง receiver func create constructor concept oop อ้างอิงไป interface ของ repository DATABASE เหมือนเป็น constructor bookService

func (s bookService) GetBookByID(bookID int) (*BookResponse, error) {
	book, err := s.bookRepository.GetBookByID(bookID)

	if err != nil {
		log.Println(err)
	}

	bookResponse := BookResponse{
		ID:          book.ID,
		Title:       book.Title,
		Author:      book.Author,
		Description: book.Description,
		Price:       book.Price,
	}

	return &bookResponse, nil
} // เชื่อมต่อ repository กับ service
func (s bookService) GetAllBooks() ([]BookResponse, error) {
	books, err := s.bookRepository.GetAllBook()
	if err != nil {
		log.Println(err)
	}

	bookResponses := []BookResponse{} //custom bookResponse
	for _, book := range books {
		bookResponse := BookResponse{
			ID:          book.ID,
			Title:       book.Title,
			Author:      book.Author,
			Description: book.Description,
			Price:       book.Price,
		}
		bookResponses = append(bookResponses, bookResponse)
	}
	return bookResponses, nil
}
func (s bookService) CreateBook(r *BookRequest) error {
	newBook := &book.Book{
		Title:       r.Title,
		Author:      r.Author,
		Description: r.Description,
		Price:       r.Price,
	}

	// ทำการบันทึกหนังสือในฐานข้อมูลหรือแหล่งข้อมูลอื่น ๆ ที่ใช้งาน
	err := s.bookRepository.CreateNewBook(newBook)

	if err != nil {
		// handle error
		log.Printf("Error creating book: %v\n", err)
		return err
	}
	return nil

}

func (s bookService) DeleteBook(bookID int) error {
	// ทำการลบหนังสือที่มี ID ตามที่ระบุในฐานข้อมูล
	err := s.bookRepository.DeleteBook(bookID)

	if err != nil {
		// แสดงข้อความแจ้งเตือนเกี่ยวกับข้อผิดพลาด
		log.Printf("Error deleting book with ID %d: %v\n", bookID, err)

		// สามารถคืนค่าข้อผิดพลาดนี้กลับไปยังผู้เรียกใช้ฟังก์ชัน
		return err
	}

	// ถ้าไม่มีข้อผิดพลาด คืนค่า nil
	return nil
}
func (s bookService) UpdateBook(bookID int, u *BookRequest) error {
	// ทำการอัปเดตหนังสือในฐานข้อมูลด้วยข้อมูลที่ให้มา

	updateBook := &book.Book{
		ID:          bookID,
		Title:       u.Title,
		Author:      u.Author,
		Description: u.Description,
		Price:       u.Price,
	}

	err := s.bookRepository.Update(bookID, updateBook)

	if err != nil {
		// แสดงข้อความแจ้งเตือนเกี่ยวกับข้อผิดพลาด
		log.Printf("Error updating book: %v\n", err)

		// สามารถคืนค่าข้อผิดพลาดนี้กลับไปยังผู้เรียกใช้ฟังก์ชัน
		return err
	}

	// ถ้าไม่มีข้อผิดพลาด คืนค่า nil
	return nil
}

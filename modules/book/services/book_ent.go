package services

// Business ควรทำ Model ใหม่ไม่เอาฝั่ง Db  ออก data transfer
type BookResponse struct {
	ID          int     `json:"id"`
	Title       string  `json:"title"`
	Author      string  `json:"author"`
	Description string  `json:"description"`
	Price       float64 `json:"price"`
}

type BookRequest struct {
	ID          int     `json:"id"`
	Title       string  `json:"title"`
	Author      string  `json:"author"`
	Description string  `json:"description"`
	Price       float64 `json:"price"`
}

// port service bussiness Logic port = interface
type BookService interface {
	CreateBook(book *BookRequest) error
	GetBookByID(bookID int) (*BookResponse, error)
	GetAllBooks() ([]BookResponse, error)
	UpdateBook(bookID int, book *BookRequest) error
	DeleteBook(bookID int) error
}

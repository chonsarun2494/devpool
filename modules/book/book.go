package book

type Book struct {
	ID          int     `db:"id"`
	Title       string  `db:"title"`
	Author      string  `db:"author"`
	Description string  `db:"description"`
	Price       float64 `db:"price"`
} //entity หรือ port เป็นฝั่งของ Database

type BookRepository interface {
	CreateNewBook(book *Book) error
	Update(bookID int, book *Book) error
	DeleteBook(bookID int) error
	GetBookByID(bookID int) (*Book, error)
	GetAllBook() ([]Book, error)
}

// สิ้นสุด ฝั่ง Database

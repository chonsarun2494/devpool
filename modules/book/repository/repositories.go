package repository

import (
	"gitlab.com/chonsarun2494/bookstore/modules/book"
	"gitlab.com/chonsarun2494/bookstore/pkg/gorm"
)

type bookRepository struct {
	db *gorm.DB
} //adaptor

func NewBookRepository(db *gorm.DB) *bookRepository {
	return &bookRepository{db: db}
} //new instance

func (r *bookRepository) CreateNewBook(book *book.Book) error {

	result := r.db.Create(&book)
	return result.Error

}

func (r *bookRepository) Update(bookID int, book *book.Book) error {
	result := r.db.Update(&book)
	return result.Error

}

func (r *bookRepository) DeleteBook(bookID int) error {
	book := &book.Book{ID: bookID}
	result := r.db.Delete(book)
	return result.Error
}

func (r *bookRepository) GetBookByID(bookID int) (*book.Book, error) {
	var book book.Book
	result := r.db.First(&book, bookID)
	if result.Error != nil {
		return nil, result.Error
	}
	return &book, nil
}

func (r *bookRepository) GetAllBook() ([]book.Book, error) {
	books := []book.Book{}
	result := r.db.Find(&books) //ส่ง instance ของ Book เป็น pointer
	if result.Error != nil {
		return nil, result.Error
	}
	return books, nil
}

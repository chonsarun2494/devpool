package handler

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/chonsarun2494/bookstore/modules/book/services"
	"gitlab.com/chonsarun2494/bookstore/util"
)

// ทำงานใน ฝั่ง adaptor ไม่ expose ต้องทำผ่าน fucn new เท่านั้น
// จะไม่อนุญาติให้ใช้ repository โดยตรง ใช้ service เท่านั้น

type bookHandler struct {
	bookService services.BookService
}

func NewBookHandler(bookService services.BookService) bookHandler {
	return bookHandler{bookService: bookService}
}

func (h bookHandler) GetAllBookHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		books, err := h.bookService.GetAllBooks()
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
		// ส่งคืนข้อมูลหนังสือเป็น JSON
		c.JSON(http.StatusOK, books)
	}
}

func (h bookHandler) GetBookByIdHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		idStr := c.Param("id")
		id, err := strconv.Atoi(idStr)
		if err != nil {
			util.SendError(c, http.StatusBadRequest, err)
			return
		}
		book, err := h.bookService.GetBookByID(id)
		if err != nil {
			util.SendError(c, http.StatusNotFound, err)
			return
		}
		c.IndentedJSON(http.StatusOK, book)
	}
}

func (h bookHandler) DeleteBookHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		idStr := c.Param("id")
		id, err := strconv.Atoi(idStr)
		if err != nil {
			util.SendError(c, http.StatusBadRequest, err)
			return
		}
		err = h.bookService.DeleteBook(id)
		if err != nil {
			util.SendError(c, http.StatusNotFound, err)
			return
		}
		c.JSON(http.StatusOK, gin.H{"message": "Book deleted successfully"})
	}
}

func (h bookHandler) NewBookHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		var req services.BookRequest

		// ทำการ bind ข้อมูลจาก request body ไปยัง struct
		if err := c.BindJSON(&req); err != nil {
			util.SendError(c, http.StatusBadRequest, err)
			return
		}

		// createBook

		if err := h.bookService.CreateBook(&req); err != nil {
			c.IndentedJSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
			})
			return
		}
		c.IndentedJSON(http.StatusOK, req)
	}
}

func (h bookHandler) UpdateBookHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		var req services.BookRequest
		idStr := c.Param("id")
		id, err := strconv.Atoi(idStr)
		if err != nil {
			util.SendError(c, http.StatusBadRequest, err)
			return
		}

		// ทำการ bind ข้อมูลจาก request body ไปยัง struct
		if err := c.BindJSON(&req); err != nil {
			util.SendError(c, http.StatusBadRequest, err)
			return
		}
		req.ID = id
		if err := h.bookService.UpdateBook(id, &req); err != nil {
			c.IndentedJSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
			})
			return
		}
		c.IndentedJSON(http.StatusOK, req)
	}

}

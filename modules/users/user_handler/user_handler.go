package user_handler

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/chonsarun2494/bookstore/modules/users"
	"gitlab.com/chonsarun2494/bookstore/util"
)

type userHanlder struct {
	usersService users.UsersService
}

func NewUsersHandler(usersService users.UsersService) userHanlder {
	return userHanlder{usersService: usersService}
}
func (h userHanlder) CheckLoginHandler() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var req users.UserLogin

		if err := ctx.BindJSON(&req); err != nil {
			util.SendError(ctx, http.StatusBadRequest, err)
			return
		} //BindJSON เก็บไว้ในตัวแปร req
		if req.Username == "" || req.Password == "" {
			util.SendError(ctx, http.StatusBadRequest, errors.New("ชื่อผู้ใช้งานหรือพาสเวริด์ต้องไม่เป็นค่าว่าง"))
			return
		}
		token, err := h.usersService.CheckLogin(&req)
		if err != nil {
			util.SendError(ctx, http.StatusUnauthorized, err)
			return
		}
		ctx.JSON(http.StatusOK, gin.H{
			"message": "เข้าสู่ระบบเรียบร้อยแล้ว",
			"token":   token,
		})

	}
}

func (h userHanlder) RegisterUser() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var req users.UserRequest

		if err := ctx.BindJSON(&req); err != nil {
			util.SendError(ctx, http.StatusBadRequest, err)
			return
		}
		if req.Username == "" || req.Password == "" {
			util.SendError(ctx, http.StatusBadRequest, errors.New("ชื่อผู้ใช้งานหรือพาสเวริด์ต้องไม่เป็นค่าว่าง"))
			return
		}

		if err := h.usersService.CreateUser(&req); err != nil {
			util.SendError(ctx, http.StatusInternalServerError, err)
			return
		}
		ctx.IndentedJSON(http.StatusOK, gin.H{
			"message": "ลงทะเบียนเรียบร้อยแล้ว",
		})

	}
}

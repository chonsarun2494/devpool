package users_repository

import (
	"errors"

	"gitlab.com/chonsarun2494/bookstore/modules/users"
	"gitlab.com/chonsarun2494/bookstore/pkg/gorm"
	"golang.org/x/crypto/bcrypt"
)

type userRepository struct {
	db *gorm.DB
}

func NewUserRepository(db *gorm.DB) *userRepository {
	return &userRepository{db: db}
}

func (r *userRepository) CreateNewUser(Users *users.Users) error {

	hash, err := bcrypt.GenerateFromPassword([]byte(Users.Password), 12)
	if err != nil {
		return err
	}
	Users.Password = string(hash)
	Users.Role = "guest"
	result := r.db.Create(&Users)
	return result.Error
}

func (r *userRepository) Update(UserID int, Users *users.Users) error {
	result := r.db.Update(&Users)
	return result.Error

}

func (r *userRepository) DeleteUser(UserID uint) error {
	User := &users.Users{ID: UserID}
	result := r.db.Delete(User)
	return result.Error
}

func (r *userRepository) GetUserByID(UserID int) (*users.Users, error) {
	var Users users.Users
	result := r.db.First(&Users, UserID)
	if result.Error != nil {
		return nil, result.Error
	}
	return &Users, nil
}

func (r *userRepository) GetAllUser() ([]users.Users, error) {
	Users := []users.Users{}
	result := r.db.Find(&Users) //ส่ง instance ของ User เป็น pointer
	if result.Error != nil {
		return nil, result.Error
	}
	return Users, nil
}

func (r *userRepository) Login(cer *users.UserLogin) (*users.Users, error) {
	var user users.Users
	result := r.db.Find(&user, "username = ?", cer.Username)

	if result.Error != nil {
		// คืนค่าข้อผิดพลาดเมื่อไม่พบข้อมูลผู้ใช้ หรือมีปัญหาอื่น ๆ
		return nil, result.Error
	}
	if result.RowsAffected == 0 {
		// คืนค่าข้อผิดพลาดเมื่อไม่พบข้อมูลผู้ใช้
		return nil, errors.New("ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง")
	}
	err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(cer.Password))
	if err != nil {
		return nil, errors.New("ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง")
	}

	// คืนค่าข้อมูลขงผู้ใช้ที่พบอ
	return &user, nil
}

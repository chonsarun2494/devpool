package users_service

import (
	"log"

	"gitlab.com/chonsarun2494/bookstore/modules/users"
	"gitlab.com/chonsarun2494/bookstore/pkg/jwt"
)

type usersService struct {
	userRepo users.UserRepository
}

func NewUsersService(userRepo users.UserRepository) usersService {
	return usersService{userRepo: userRepo}
}

func (s usersService) CreateUser(r *users.UserRequest) error {
	newUser := &users.Users{
		Username: r.Username,
		Password: r.Password,
	}
	err := s.userRepo.CreateNewUser(newUser)
	if err != nil {
		log.Printf("Error creating User: %v\n", err)
		return err
	}

	return nil
}
func (s usersService) CheckLogin(r *users.UserLogin) (string, error) {
	user, err := s.userRepo.Login(r)
	if err != nil {
		log.Println("Login failed:", err)
		return "", err
	}

	token, err := jwt.GenerateToken(user.ID, user.Role)
	if err != nil {
		log.Println("can't generate token", err)
		return "", err
	}

	return token, nil
}

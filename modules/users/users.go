package users

type Users struct {
	ID       uint   `db:"id" gorm:"primaryKey;autoIncrement"`
	Username string `db:"username" gorm:"uniqueIndex;not null"`
	Password string `db:"password"`
	Role     string `db:"role"`
}

// users repository interface
type UserRepository interface {
	CreateNewUser(User *Users) error
	Update(UserID int, User *Users) error
	DeleteUser(UserID uint) error
	GetUserByID(UserID int) (*Users, error)
	GetAllUser() ([]Users, error)
	Login(User *UserLogin) (*Users, error)
}

// register and login
type UserRequest struct {
	ID       uint   `db:"id" gorm:"primaryKey"`
	Username string `db:"username" gorm:"uniqueIndex;not null"`
	Password string `db:"password"`
}
type UserLogin struct {
	Username string `db:"username" gorm:"uniqueIndex;not null"`
	Password string `db:"password"`
}

// service
type UsersService interface {
	CreateUser(User *UserRequest) error
	CheckLogin(User *UserLogin) (string, error)
}

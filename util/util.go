package util

import (
	"log"

	"github.com/gin-gonic/gin"
)

func SendError(c *gin.Context, status int, err error) {
	log.Println(err)

	if err.Error() == "ERROR: duplicate key value violates unique constraint \"idx_users_username\" (SQLSTATE 23505)" {
		c.JSON(status, gin.H{
			"message": "ชื่อผู้ใช้งานนี้ถูกใช้ไปแล้ว",
		})
	} else {
		c.JSON(status, gin.H{
			"message": err.Error(),
		})
	}

}
